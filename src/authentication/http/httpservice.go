package http

import (
	"StackWebAPI/src/data"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"time"
	"strings"
	auth "StackWebAPI/src/authentication"
)

type HttpService struct {
	auth    *auth.Authentication
	logger  *log.Logger
	signkey string
}

type Credential auth.Credential

func NewHttpService(dataStore *data.DataStore, signkey string, logger *log.Logger) *HttpService {
	result := &HttpService{}
	result.auth = auth.NewAuth(dataStore, logger)
	result.logger = logger
	result.signkey = signkey
	return result
}

func JsonEncode(data interface{}) []byte {
	result,_:= json.Marshal(data);
	return result;
}
func JsonEncodeToString(data interface{}) string {
	result:=string(JsonEncode(data));
	return result;
}

func GetErrorResult(code int, description string) string {
	return JsonEncodeToString(struct {
		Successful bool
		Code int
		Description string
	} {
		false,
		code,
		description,
	});
}
func GetSuccessResult(key string, value interface{}) string {
	encoded:=JsonEncodeToString(struct {
		Successful bool
		Data interface{}
	}{
		true,
		value,
	});
	result:= strings.Replace(encoded,"Data",key,1);
	return result;
}

func (this *HttpService) RegisterUserHandler(responseWriter http.ResponseWriter, request *http.Request) {
	this.logger.Println("Request Recieved")
	responseData:= "";
	jsonDecoder := json.NewDecoder(request.Body)
	defer request.Body.Close()
	var cred *Credential = &Credential{}
	err := jsonDecoder.Decode(cred)
	if err != nil {
		responseWriter.WriteHeader(http.StatusBadRequest)
		responseData=GetErrorResult(http.StatusBadRequest,"Could not decode")
		this.logger.Println("Could not decode \n", err.Error())
	} else if !this.auth.RegisterUser(auth.Credential(*cred)) {
		responseWriter.WriteHeader(http.StatusBadRequest)
		responseData=GetErrorResult(http.StatusBadRequest,"Was unable to register user")
		this.logger.Println("Was unable to register user")
	} else{
		this.logger.Println("DB recored entered successfully")
		responseData = GetSuccessResult("Token",this.getJwtToken(cred))
		this.logger.Println("Token generated:", responseData)
	}
	fmt.Fprintln(responseWriter, responseData)
}

func (this *HttpService) AuthenticateUserHandler(responseWriter http.ResponseWriter, request *http.Request) {
	responseData:="";
	this.logger.Println("Request Recieved")
	jsonDecoder := json.NewDecoder(request.Body)
	defer request.Body.Close()
	cred := &Credential{}
	err := jsonDecoder.Decode(cred)
	if err != nil {
		responseWriter.WriteHeader(http.StatusBadRequest)
		responseData=GetErrorResult(http.StatusBadRequest,"Could not decode:\n"+err.Error());
		this.logger.Println("Could not decode \n", err.Error())
	} else if isvalid, err := this.auth.AuthenticateUser(auth.Credential(*cred)); !isvalid {
		responseWriter.WriteHeader(http.StatusBadRequest)
		responseData=GetErrorResult(http.StatusBadRequest,err.Error());
		this.logger.Println(err.Error())
	} else {
		responseData =  GetSuccessResult("Token",this.getJwtToken(cred))
	}
	fmt.Fprintln(responseWriter, responseData)
}

//Middleware for ensuring that token provided is valid
func (this *HttpService) VerifyTokenHandler (nextHandler http.Handler) http.Handler {
	return http.HandlerFunc ( func (responseWriter http.ResponseWriter, request *http.Request) {
		authHeader:=request.Header.Get("Authorization");
		token:= strings.Trim(strings.Trim(authHeader, "Bearer")," ");
		this.logger.Println("Token:",token);
		if ok,err:=this.parseJwtToken(token); ok {
			nextHandler.ServeHTTP(responseWriter,request);
		} else {
			responseWriter.WriteHeader(http.StatusUnauthorized);
			fmt.Fprintln(responseWriter, GetErrorResult(http.StatusUnauthorized, err.Error()));
		}
	});
}

func (this *HttpService) getJwtToken(cred *Credential) string {
	duration := cred.DurationMins
	if duration == 0 {
		duration = 525600 // 1 year
	}
	exp := time.Now().Add(time.Duration(duration) * time.Minute).Unix();
	nbf := time.Now().Unix();
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp":  exp,
		"nbf":	nbf,
		"iat":  nbf,
		"email": cred.Email,
	})
	tokenString, err := token.SignedString([]byte(this.signkey))
	if err != nil {
		this.logger.Println(err.Error())
	}
	return tokenString
}

type Claims struct {
	jwt.StandardClaims
	Email string `json:"email"`
}
func (this *HttpService) parseJwtToken(tokenString string) (bool,error){
	claims:=&Claims{};
	token, err:= jwt.ParseWithClaims(tokenString,claims, func(token *jwt.Token)(interface{},error){
		if _,ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			fmt.Println("Key error occured");
			return nil,  fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return this.signkey, nil
	});
	this.logger.Println(token.Valid)
	this.logger.Println(claims.ExpiresAt)
	result:= err==nil && token.Valid && claims.ExpiresAt > time.Now().Unix();
	return result,err;
}