package authentication

import (
	"StackWebAPI/src/data"
	"crypto/sha1"
	"errors"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/pbkdf2"
	"log"
	"time"
)

type User struct {
	gorm.Model
	Password        string `gorm:"size:100"`
	ResetToken      string
	TokenExpiryDate time.Time
	Email           string
	Telephone       string
}

type Credential struct {
	Email        string
	Password     string
	DurationMins int
}

type Authentication struct {
	DB     *gorm.DB
	Logger *log.Logger
}

func NewAuth(dataStore *data.DataStore, logger *log.Logger) *Authentication {
	result := &Authentication{}
	result.initialize(dataStore, logger)
	return result
}
func (this *Authentication) initialize(dataStore *data.DataStore, logger *log.Logger) {
	this.DB = dataStore.Db
	this.DB.AutoMigrate(User{})
}
func (this *Authentication) RegisterUser(credential Credential) bool {
	email := credential.Email
	password := credential.Password
	hash := getHash(password, email)
	user := User{Email: email, Password: string(hash[:])}
	existing := &User{}
	if this.DB.First(existing, "email=?", email); this.DB.NewRecord(*existing) {
		this.DB.Create(&user)
	}
	return !this.DB.NewRecord(user)
}

func (this *Authentication) AuthenticateUser(credential Credential) (bool, error) {
	email := credential.Email
	password := credential.Password
	result := false
	var err error = nil
	queryResult := &User{}
	this.DB.First(queryResult, "Email= ?", email)
	if queryResult == nil {
		err = errors.New("Username is incorrect")
	} else if queryResult != nil && getHash(password, email) != queryResult.Password {
		err = errors.New("Password is incorrect")
	} else {
		result = true
	}
	return result, err
}
func getHash(password string, salt string) string {
	hash := pbkdf2.Key([]byte(password), []byte(salt), 4096, 32, sha1.New)
	return string(hash)
}
