package main

import (
	authhttp "StackWebAPI/src/authentication/http"
	"StackWebAPI/src/data"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

const SIGKEY string = "secret signing key"

func main() {
	service := "localhost:8080"
	if len(os.Args) == 2 {
		service = os.Args[1]
	}
	mux := mux.NewRouter()
	logger := log.New(os.Stdout, "", log.LstdFlags)
	dataStore := data.NewDataStore("storage.db", logger)
	defer dataStore.Close()
	registerRoutes(mux)
	initAuthServices(mux, dataStore, logger)
	server := http.Server{
		Addr:    service,
		Handler: mux,
	}
	if err := server.ListenAndServe(); err != nil {
		fmt.Fprint(os.Stderr, err.Error())
		os.Exit(-1)
	}
}

func registerRoutes(mux *mux.Router) {
}

type IndexHandler struct{}

func (IndexHandler) ServeHTTP(responseWriter http.ResponseWriter, request *http.Request) {
	fmt.Fprintln(responseWriter, "<h1>It works!</h1>")
}
func initAuthServices(mux *mux.Router, dataStore *data.DataStore, logger *log.Logger) {
	authService := authhttp.NewHttpService(dataStore, SIGKEY, logger)
	mux.HandleFunc("/auth/v1.0/register", authService.RegisterUserHandler).Methods("POST")
	mux.HandleFunc("/auth/v1.0/authenticate", authService.AuthenticateUserHandler).Methods("POST")
	mux.Handle("/index", authService.VerifyTokenHandler(IndexHandler{}))
}
