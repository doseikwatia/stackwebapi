package data

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
	"os"
)

type DataStore struct {
	filename string
	Db       *gorm.DB
	logger   *log.Logger
}

func (data *DataStore) initialize(filename string, logger *log.Logger) {
	if _, err := os.Stat(filename); err != nil && os.IsNotExist(err) {
		filename = "data.db"
	}
	db, err := gorm.Open("sqlite3", filename)
	if err != nil {
		logger.Println("Could not open ", filename)
		logger.Fatalln(err)
	}
	data.filename = filename
	data.Db = db
	data.logger = logger
}

func NewDataStore(filename string, logger *log.Logger) *DataStore {
	result := &DataStore{}
	result.initialize(filename, logger)
	return result
}

func (data *DataStore) Close() error {
	err := data.Db.Close()
	return err
}
